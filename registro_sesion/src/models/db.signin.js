require('dotenv').config();
const mongoose = require("mongoose");
const signinSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    surname: {
        type: String,
        required: true,
    },
    email: { 
        type: String,
        required: true,
    },
    password: { 
        type: String,
        required: true,
    },
    password2: { 
        type: String,
        required: true,
    }
});

module.exports = mongoose.model('signin', signinSchema);