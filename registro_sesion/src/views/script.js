//const msj = document.getElementById('msj');
const app_port = 4000;



/* const callUsers = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");


    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`http://localhost:${app_port}/usuarios`, requestOptions)
        .then(response => response.text())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;
} */



const newUser = async (dataInput) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let data = {
        name: dataInput.name,
        surname: dataInput.surname,
        email: dataInput.email,
        password: dataInput.password,
        password2: dataInput.password2
    }
    console.log(data);

    let requestOptions = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: myHeaders,
        redirect: 'follow'
    };
    console.log(requestOptions);

    await fetch(`http://localhost:${app_port}/signin`, requestOptions)
        .then(response => response.text())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;
}

/* const showUsers = async () => {
    let nombres = '';
    let data = await callUsers();
    data = JSON.parse(data);
    for (let i = 0; i < data.length; i++) {
        if (i == 0) {
            nombres += ' ' + data[i].name;
        } else if (i + 1 == data.length) {
            nombres += ' y ' + data[i].name
        } else {
            nombres += ', ' + data[i].name
        }
    }
    return nombres;
}


const deleteAll = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`http://localhost:${app_port}/usuarios`, requestOptions)
        .then(response => response.text())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;
}

const deleteUser = async (name) => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
    };

    await fetch(`http://localhost:${app_port}/usuarios/${name}`, requestOptions)
        .then(response => response.text())
        .then(result => resp = result)
        .catch(error => console.log('error', error));
    return resp;
} */

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function onSignFb(){
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}
