
const signinSchema = require("../models/db.signin");

//CREAR USUARIO
const newUser = async (req) => {
    const temp = await new  signinSchema({
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        password: req.body.password,
        password2: req.body.password2
    })
    const result = await temp.save();
    return result;
}

module.exports = {
    newUser
}