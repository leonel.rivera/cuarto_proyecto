const dotenv = require('dotenv').config();
console.log(process.env)
const conectionString = process.env.MONGO_CONN_STR;
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 4000;

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use(express.json());
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

const login = require("./src/routes/routes.login");
app.use("/", login);

const signin = require('./src/routes/routes.signin');
app.use("/", signin);

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`)
})

async function main() {
    await mongoose
        .connect(conectionString, options)
        .then(() => console.log("Connected to MongoDB Atlas"))
        .catch((error) => console.error(error));
}
main().catch(err => console.log(err));
module.exports = mongoose;